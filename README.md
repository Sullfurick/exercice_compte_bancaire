# LE FICHIER CONTENANT LE CODE DE L'EXERCICE SE TROUVE DANS LA BRANCHE MASTER



# Exercice Python : Développement d'un programme de création/gestion de comptes bancaires.



## Objectifs

Le sujet principal est de développer un programme orienté objet, pour gérer et créer des comptes courants ainsi que des livrets épargnes.
Le plus important est d'avoir compris l'objet ainsi que les bases de Python.

## Méthodes

Le programme doit contenir des méthodes de retrait et de versement, qui seront surchargées dans les classes filles pour gérer
 l'application d'agios dans le cadre du compte courant, ainsi que l'application d'intérêts dans le cadre du compte épargne.


## Classes

-  Classe abstraite Compte
-  Classe CompteCourant
-  Classe CompteEpargne


## Auteur 
Orel'/Sullfurick


## Statut du projet
Ce projet est réalisé dans le cadre du cours de Développement Python de @docusland.\
Ce projet est prêt à être livré mais est largement améliorable de nombreuses façons. 
